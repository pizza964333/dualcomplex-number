module Data.DualComplex where

import System.Random
import System.IO.Unsafe

data DualComplex a = DC { real :: a, imag :: a, epsi :: a, eimg :: a }
 deriving (Show,Read,Eq)

instance Num a => Num (DualComplex a) where
  (+) (DC w i e p) (DC v h d q) = DC (w+v) (i+h) (e+d) (p+q)
  (*) (DC w i e p) (DC v h d q) = DC (w*v - i*h) (w*h + i*v) (w*d - i*q + e*v - p*h) (w*q + i*d + e*h + p*v)
  fromInteger n = DC (fromInteger n) 0 0 0

dcs = map (\_ -> unsafePerformIO $ randomDualComplex) [0..]

dc n = dcs !! n

randomDualComplex :: IO (DualComplex Double)
randomDualComplex = do
  w <- randomRIO (-2,2)
  e <- randomRIO (-2,2)
  i <- randomRIO (-2,2)
  p <- randomRIO (-2,2)
  return $ DC w e i p

magnitude :: (RealFloat a) => DualComplex a -> a
magnitude (DC x y _ _) =  scaleFloat k
                     (sqrt (sqr (scaleFloat mk x) + sqr (scaleFloat mk y)))
                    where k  = max (exponent x) (exponent y)
                          mk = - k
                          sqr z = z * z

magnitude2 :: (RealFloat a) => DualComplex a -> a
magnitude2 (DC _ _ x y) =  scaleFloat k
                     (sqrt (sqr (scaleFloat mk x) + sqr (scaleFloat mk y)))
                    where k  = max (exponent x) (exponent y)
                          mk = - k
                          sqr z = z * z

magnitude3 :: (RealFloat a) => DualComplex a -> a
magnitude3 (DC w i e p) =  scaleFloat k
                     (sqrt (sqr (scaleFloat mk w) + sqr (scaleFloat mk i) + sqr(scaleFloat mk e) + sqr(scaleFloat mk p)))
                    where k  = foldr1 max [exponent w, exponent i, exponent e, exponent p]
                          mk = - k
                          sqr z = z * z

{-

(w + i + e + p) * (v + h + d + q)

w*v + w*h + w*d       + i*v + i*h + i*d +   e*v + e*h + e*d
real  imag  nilp        imag -real  ni*im   nilp  ni*im  0

       w*v + w*h + w*d + w*q + i*v + i*h + i*d + i*q + e*v + e*h + e*d + e*q + p*v + p*h + p*d + p*q
       real  imag  epsi  eimg  img  -real  eimg -epsi  epsi  eimg  0     0     eimg -epsi  0     0

real   w*v                         - i*h
imag         w*h             + i*v                           
epsi               w*d                         - i*q + e*v                         - p*h
eimg                     w*q             + i*d             + e*h             + p*v

-}
                                                                                                

{-

real * real =  real
imag * imag = -real
real * imag =  imag

nilp * nilp = 0

nilp * imag * imag = -nilp
nilp * real        =  nilp
nilp * imag * real = nilp * imag
nilp * imag        = nilp * imag

-}
